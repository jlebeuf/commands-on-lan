﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CommandsOnLAN
{
    static class OutputToTextbox
    {
        private static TextBox textBox = null;

        public static void setTextBox(TextBox tb)
        {
            textBox = tb;
        }

        public static void Write(string str)
        {
            if (textBox == null)
                return;
            textBox.AppendText(str);
        }

        public static void WriteLine(string str)
        {
            if (textBox == null)
                return;
            textBox.AppendText(str + "\n");
        }
    }
}
