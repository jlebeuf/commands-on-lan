﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandsOnLAN
{
    class Command
    {
        public readonly string name;
        public readonly byte[] packet;
        public readonly string command;
        public readonly string arguments;

        public Command(string name, byte[] packet, string command, string arguments)
        {
            this.name = name;
            this.packet = new byte[packet.Length];
            for (int i = 0; i < packet.Length; i++)
                this.packet[i] = packet[i];
            this.command = command;
            this.arguments = arguments;
        }
    }
}
