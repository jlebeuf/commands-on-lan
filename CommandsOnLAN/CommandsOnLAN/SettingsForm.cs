﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CommandsOnLAN
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
            listenPortTextBox.Text = Properties.Settings.Default.ListenPort.ToString();
            useStubLauncherCheckBox.Checked = Properties.Settings.Default.UseStubLauncher;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            int listenPort;
            bool save = false;
            if (Int32.TryParse(listenPortTextBox.Text, out listenPort))
            {
                Properties.Settings.Default.ListenPort = (uint)listenPort;
                save = true;
            }
            if (Properties.Settings.Default.UseStubLauncher != useStubLauncherCheckBox.Checked)
            {
                Properties.Settings.Default.UseStubLauncher = useStubLauncherCheckBox.Checked;
                save = true;
            }

            if (save)
                Properties.Settings.Default.Save();
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
