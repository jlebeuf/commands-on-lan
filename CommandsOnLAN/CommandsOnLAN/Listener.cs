﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CommandsOnLAN
{
    class Listener
    {
        private readonly int listenPort;
        private readonly Command[] commands;
        private UdpClient listener;
        private IPEndPoint groupEP;
        Thread thread;
        private bool continueListen;

        public class ListenerCommandEvent : EventArgs
        {
            public readonly string message;
            public ListenerCommandEvent(string msg)
            {
                message = msg;
            }
        }
        public event ListenerCommandHandler ListenerCommand;
        public delegate void ListenerCommandHandler(Listener l, ListenerCommandEvent e);


        public Listener(int listenPort, Command[] commands)
        {
            this.listenPort = listenPort;
            this.commands = commands;
            listener = new UdpClient(listenPort);
            groupEP = new IPEndPoint(IPAddress.Any, listenPort);

            thread = new Thread(new ThreadStart(ThreadProc));
            thread.Priority = ThreadPriority.BelowNormal;
            thread.IsBackground = true;
            continueListen = false;
        }

        public void Start()
        {
            continueListen = true;
            thread.Start();
        }

        public void Kill()
        {
            continueListen = false;
            //thread.Abort();
        }

        private void ThreadProc()
        {
            try
            {
                ListenerCommand(this, new ListenerCommandEvent("Waiting for broadcast" + Environment.NewLine));
                while (continueListen)
                {
                    if (listener.Available == 0)
                        goto sleep;

                    byte[] bytes = listener.Receive(ref groupEP);
                    ListenerCommand(this, new ListenerCommandEvent(DateTime.Now.ToString("G") + Environment.NewLine + " Received broadcast from " + groupEP.ToString() + Environment.NewLine + BitConverter.ToString(bytes).Replace("-", " ") + Environment.NewLine));

                    foreach (Command com in commands)
                    {
                        if (bytes.Length == com.packet.Length)
                        {
                            for (int i = 0; i < bytes.Length; i++)
                            {
                                if (bytes[i] != com.packet[i])
                                    break;
                                if (i == bytes.Length - 1)
                                {
                                    ListenerCommand(this, new ListenerCommandEvent("Executing " + com.name + " command" + Environment.NewLine));
                                    if (Properties.Settings.Default.UseStubLauncher)
                                    {
                                        ProcessStartInfo procStartInfo = new ProcessStartInfo(".\\StubLauncher.exe", "\"" + com.command + "\" " + com.arguments);
                                        Process.Start(procStartInfo);
                                    }
                                    else
                                    {
                                        ProcessStartInfo procStartInfo = new ProcessStartInfo(com.command, com.arguments);
                                        Process.Start(procStartInfo);
                                    }
                                }
                            }
                        }
                    }


                    ListenerCommand(this, new ListenerCommandEvent(Environment.NewLine + Environment.NewLine + "Waiting for broadcast" + Environment.NewLine));


                    //////////
                    // Thread sleep
                    /////
                sleep:
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                ListenerCommand(this, new ListenerCommandEvent(e.ToString()));
            }
            finally
            {
                listener.Close();
            }
        }
    }
}
