﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace StubLauncher
{
    class Program
    {
        static void Main(string[] args)
        {
            string arg = "";
            foreach (string s in args)
                arg += s;
            ProcessStartInfo procStartInfo = new ProcessStartInfo(arg);
            Process.Start(procStartInfo);
            /*for (int i = 0; i < args.Length; i++)
                Console.WriteLine(args[i]);
            Console.ReadKey();*/
        }
    }
}
